extends Node2D

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	seed("icecube V2.0".hash())
	randomize()
	$AnimationPlayer.play("oscillation")
	$AnimationPlayer.seek(rand_range(0,2))


func _on_AnimationPlayer_animation_finished(anim_name: String) -> void:
	$AnimationPlayer.play(anim_name)
