extends KinematicBody2D

onready var player = $Sprite
const MOVE_SPEED = 100
const GRAVITY = 15
const JUMP = -300
var velocity : Vector2 = Vector2.ZERO


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(_delta: float) -> void:
	velocity.y += GRAVITY
#	# Gauche
	if Input.is_action_pressed("ui_left"):
		velocity.x = -MOVE_SPEED
	# Droite
	if Input.is_action_pressed("ui_right"):
		velocity.x = MOVE_SPEED
	# R.A.Z
	if Input.is_action_just_released("ui_left") or Input.is_action_just_released("ui_right"):
		velocity.x = 0

	# Saut
	if is_on_floor():
		# Si aucunes actions sur le sol, alors on lance l'animation "IDLE"
		if velocity.x == 0:
			$AnimationPlayer.play("Idle")
		else:
			$AnimationPlayer.stop()

		if Input.is_action_just_pressed("ui_accept"):
			velocity.y = JUMP
	# Déplace le joueur
	velocity = move_and_slide(velocity, Vector2.UP)
