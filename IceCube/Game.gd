extends Node

onready var num_room: int = 0

func erase_current_scene() -> void:
	var current_scn = "Room"+String(num_room)
	# Supprime l'ancienne scène avant de charger la suivante
	var ret = get_tree().root.has_node(current_scn)
	if ret != false:
		get_tree().root.get_node(current_scn).queue_free()

func _input(event: InputEvent) -> void:
	if event.is_action_pressed("ui_f"):
		OS.window_fullscreen = !OS.window_fullscreen
	if event.is_action_pressed("ui_cancel"):
		# Supprime la scene actuelle avant de charger la suivante
		var nde:Node = get_node("Room"+String(num_room))
		nde.queue_free()
		var _res = get_tree().change_scene("res://Menu/Menu.tscn")

func next_scene() -> void:
	erase_current_scene()
	num_room += 1
	# Charge la prochaine scène
	var name_scn: String= "res://Rooms/Room"+String(num_room)+"/Room"+String(num_room)+".tscn"
	var next_scn = load(name_scn).instance()
	print(next_scn)
	add_child(next_scn)


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	next_scene()

