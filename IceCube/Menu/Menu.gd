extends Node2D

onready var loop_music: bool = true
onready var select_item:int = 1
# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	$Particles2D.show()
	if not OS.is_debug_build():
		$Music.play()

func _input(event: InputEvent) -> void:
	if event.is_action_pressed("ui_f"):
		OS.window_fullscreen = !OS.window_fullscreen
	if event.is_action_pressed("ui_up") or event.is_action_pressed("ui_down"):
		# Inverse les valeurs de "glow"
		var g: int = $Labels/Start2.modulate.g
		var b: int = $Labels/Start2.modulate.b
		$Labels/Start2.modulate.g = $Labels/Exit2.self_modulate.g
		$Labels/Start2.modulate.b = $Labels/Exit2.self_modulate.b
		$Labels/Exit2.self_modulate.g = g
		$Labels/Exit2.self_modulate.b = b
		select_item = -select_item

	if event.is_action_pressed("ui_cancel"):
		get_tree().quit(0)
	if event.is_action_pressed("ui_accept"):
		if select_item == -1 :
			get_tree().quit(0)
		else: # Lance le jeu
			$Music.stop()
			loop_music = false
			SceneTransition.scene_transition("res://Game.tscn")

#			var ret = get_tree().change_scene("res://Game.tscn")
#			if ret == ERR_CANT_OPEN:
#				OS.alert("Can't open file", "ERROR")
#				get_tree().quit(1) # Sortie avec erreur

func _on_Music_finished() -> void:
	if loop_music:
		$Music.play()
