extends CanvasLayer

onready var colorrect: ColorRect = get_node("ColorRect")
onready var animation: AnimationPlayer = $AnimationPlayer

func scene_transition(target: String, style: String="fade") -> void:
	if style == "fade":
		transition_fade(target)

func transition_fade(scene: String) -> void:
	animation.play("fade_out")
	yield(animation, "animation_finished")
	var _ret = get_tree().change_scene(scene)
	animation.play("fade_in")
